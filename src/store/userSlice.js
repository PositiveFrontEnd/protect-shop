import { createSlice } from "@reduxjs/toolkit";
import { API_URL } from "../components/Helpers/Api/Api";
import sendRequest from "../components/Helpers/SendRequest/sendRequest";
import { actionIsAnimation } from "./homeSlice";
const userSlice = createSlice({
  name: "user",
  initialState: {
    token: "",
  },
  reducers: {
    actionToken: (state, { payload }) => {
      state.token = payload;
    },
  },
});

export const { actionToken } = userSlice.actions;

export const actionCorrectLogin = (data) => async (dispatch) => {
  dispatch(actionIsAnimation(true));
  const { loginOrEmail, password } = data;
  const userLogin = {
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      loginOrEmail: `${loginOrEmail}`,
      password: `${password}`,
    }),
  };

  await sendRequest(`${API_URL}/customers/login`, "POST", userLogin)
    .then((response) => {
      if (response) {
        dispatch(actionToken(response.token));
      } else {
        console.log("переводжу на регістрацію");
      }
    })
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};

export const actionRegistration = (customer) => async (dispatch) => {
  dispatch(actionIsAnimation(true));
  const newCustomer = {
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(customer),
  };
  await sendRequest(`${API_URL}/customers`, "POST", newCustomer)
    .then((response) => {
      console.log("Успішна відповідь:", response);
    })
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};

export const actionDeleteCard = (id) => (dispatch) => {
  dispatch(actionIsAnimation(true));

  const createCardRequest = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  sendRequest(`${API_URL}/cart/${id}`, "DELETE", createCardRequest)
    .then((response) => {
      if (response.status === 200) {
        console.log(response);
      } else {
        throw new Error("Помилка видалення картки");
      }
    })
    .catch((error) => {
      console.log(error.message);
    });
  dispatch(actionIsAnimation(false));
};

export const actionNewProduct = (data) => async (dispatch) => {
  dispatch(actionIsAnimation(true));
  const { token, product } = data;
  const newProduct = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `${token}`,
    },
    body: JSON.stringify(product),
  };
  dispatch(actionIsAnimation(true));
  await sendRequest(`${API_URL}/products`, "POST", newProduct)
    .then((response) => console.log(response))
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};

export default userSlice.reducer;
