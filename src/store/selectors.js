export const selectorAnimation = (state) => state.home.isAnimation;
export const selectorToken = (state) => state.user.token;
export const selectorsProducts = (state) => state.home.products;
export const selectorCard = (state) => state.home.card;
