import { configureStore } from "@reduxjs/toolkit";
import favoriteReducer from "./favoriteSlice.js";
import basketReducer from "./basketSlice.js";
import homeReducer from "./homeSlice.js";
import userReducer from "./userSlice.js";
export default configureStore({
  reducer: {
    favorite: favoriteReducer,
    basket: basketReducer,
    home: homeReducer,
    user: userReducer,
  },
});
