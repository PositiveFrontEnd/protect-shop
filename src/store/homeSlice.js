import { createSlice } from "@reduxjs/toolkit";
import { API_URL } from "../components/Helpers/Api/Api";
import sendRequest from "../components/Helpers/SendRequest/sendRequest";
const homeSlice = createSlice({
  name: "home",
  initialState: {
    isAnimation: false,
    products: [],
    card: {},
  },
  reducers: {
    actionIsAnimation: (state, { payload }) => {
      state.isAnimation = payload;
    },
    actionProducts: (state, { payload }) => {
      state.products = payload;
    },
    actionCard: (state, { payload }) => {
      state.card = payload;
    },
  },
});

export const { actionIsAnimation, actionProducts, actionCard } =
  homeSlice.actions;

export const actionGetProductCard = (id) => async (dispatch) => {
  dispatch(actionIsAnimation(true));
  await sendRequest(`${API_URL}/products/${id}`)
    .then((response) => {
      if (response) {
        dispatch(actionCard(response));
      }
    })
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};

export const actionGetProducts = () => async (dispatch) => {
  dispatch(actionIsAnimation(true));
  await sendRequest(`${API_URL}/products`)
    .then((response) => {
      if (response) {
        dispatch(actionProducts(response));
      }
    })
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};

export const actionPutProduct = (data) => async (dispatch) => {
  dispatch(actionIsAnimation(true));
  const { id, product } = data;
  const newProduct = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(product),
  };
  await sendRequest(`${API_URL}/products/${id}`, "PUT", newProduct)
    .then((response) => {
      if (response) {
        console.log(response);
      }
    })
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};

export const actionFilter = () => async (dispatch) => {
  dispatch(actionIsAnimation(true));

  await sendRequest(`${API_URL}/products/filter?size=small`)
    .then((response) => console.log(response))
    .catch((error) => {
      console.error("Помилка:", error);
      if (error.response) {
        console.error("Деталі помилки:", error.response.response);
      }
    });
  dispatch(actionIsAnimation(false));
};
export default homeSlice.reducer;
