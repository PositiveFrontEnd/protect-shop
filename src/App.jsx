import { React } from "react";
import "./Reset.css";
import "./components/Helpers/Base/Base.scss"
import "./components/Helpers/Variables/Variables.scss"
import "./App.css";
import AppRouters from "./AppRouters";

const App = () => {
    return <>
        <AppRouters />
    </>;
};

export default App;
