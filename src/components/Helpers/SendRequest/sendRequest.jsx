import React from "react";
const sendRequest = async (url, method = "GET", options) => {
    return await fetch(url, { method: method, ...options }).then((response) => {
        if (response.ok) {
            if (method === "DELETE") {
                return response;
            }
            return response.json();
        }
    });
}


export default sendRequest